<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Speedland / formularz zgłoszeniowy</title> 
        <meta name="description" content="Uzupełnij formularz i zgłoś swój udział w Speedland Krosno 2017!" />
        <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"/>
        <link href="https://fonts.googleapis.com/css?family=Oswald:400,700&amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="assets/styles.css?ver=13"/>
        <link rel="icon" type="image/png" href="favico.png">
        <meta property="og:image" content="http://speedland.pl/assets/img/fb-post.jpg?ver=5" />
        <meta property="og:description" content="Uzupełnij formularz i zgłoś swój udział w Speedland Krosno 2017!" />
        <meta property="og:url"content="http://speedland.pl/" />
        <meta property="og:title" content="Speedland / formularz zgłoszeniowy" />
    </head>
    <body>
        <div class="spinner">
            <img src="assets/img/spinner.png" alt="alt" />
        </div>
        <header class="slider"></header>
        <div class="container">
            <div class="messager-errors" id="message">
                    <div class="success" style="<?php echo@$_SESSION['success'] ? '' : 'display: none;' ?>">
                        <img src="/assets/img/ok-icon.png" alt="alt" />
                        <h3>Dziękujemy za zgłoszenie</h3>
                        <p>Skontaktujemy się z właścicielami wybranych aut</p>
                    </div>
                <?php if (@$_SESSION['error']) : ?>
                    <div class="error">
                        <p><?php echo @$_SESSION['error']; ?></p>
                    </div>
                <?php endif ?>
                <?php @session_destroy(); ?>
            </div>
            <div class="form-col">
                <h2>Zgłoś swoje auto na Speedland Krosno 2017</h2>
                <form class="form" action="sender.php" method="post" enctype="multipart/form-data">
                    <div class="appointment-type" id="appointment-type-trigger">
                        <label for="type">Biorę udział w:</label>
                        <div class="blocked-input checked">
                            <input type="radio" name="type" value="drift" id="drift-trigger" class="blocked-input" checked>
                        </div>
                        <div class="blocked-input">
                            <input type="radio" name="type" value="zlot" id="zlot-trigger">
                        </div>
                    </div>
                    
                    <label for="category" class="zlot">Kategoria:</label>
                    <div class="select-wrapper zlot">
                        <select class="zlot" name="category" id="category-select">
                            <option value="Classic">Classic</option>
                            <option value="4x4">4X4</option>
                            <option value="Exotic Cars">Exotic Cars</option>
                            <option value="Tuning">Tuning</option>
                        </select>
                    </div>
                    <label for="name">Imię*:</label>
                    <input type="text" name="name" class="text-input required" required="required">
                    <label for="surname">Nazwisko*:</label>
                    <input type="text" name="surname" class="text-input required" required="required">
                    <label for="email">Adres email*:</label>
                    <input type="email" name="email" class="text-input required" required="required">
                    <label for="email2">Powtórz adres email*:</label>
                    <input type="email" name="email2" class="text-input required" required="required">
                    <label for="phone">Telefon*:</label>
                    <input type="tel" name="phone" class="text-input required" required="required">
                    <label for="facebook" class="zlot">Profil Facebook:</label>
                    <input type="text" name="facebook" class="text-input zlot">
                    <label for="city" class="zlot">Miasto*:</label>
                    <input type="text" name="city" class="text-input zlot required">
                    <label for="plate" class="zlot">Numer rejestracyjny*:</label>
                    <input type="text" name="plate" class="text-input zlot required">
                    <label for="team" class="drift">Team:</label>
                    <label for="team" class="zlot">Klub/ekipa:</label>
                    <p class="smaller">Profil Facebook, fanpage lub strona www</p>
                    <input type="text" name="team" class="text-input">
                    <label for="city" class="drift">Miasto*:</label>
                    <input type="text" name="city" class="text-input drift required" required="required">
                    <label for="car">Marka samochodu*:</label>
                    <input type="text" name="car" class="text-input required" required="required">
                    <label for="carmodel">Model samochodu*:</label>
                    <input type="text" name="carmodel" class="text-input required" required="required">
                    <label for="production-year" class="zlot">Rok produkcji*:</label>
                    <input type="text" name="production-year" class="text-input zlot required">
                    
                    <div class="drift radio-wrapper">
                        <div id="turbo">
                            <label for="turbo">Turbo*:</label>
                            <div class="radio-input-wrapper checked">
                                <input type="radio" name="turbo" value="tak" class="normal-radio-input" checked required="required" id="turbo-true">
                            </div>
                            <p class="radio-text">Tak</p>
                            <div class="radio-input-wrapper">
                                <input type="radio" name="turbo" value="nie" class="normal-radio-input" id="turbo-false">
                            </div>
                            <p class="radio-text">Nie</p>
                        </div>
                        <div id="nitro">
                            <label for="nitro">Nitro*:</label>
                            <div class="radio-input-wrapper checked">
                                <input type="radio" name="nitro" value="tak" class="normal-radio-input" checked required="required" id="nitro-true">
                            </div>
                            <p class="radio-text">Tak</p>
                            <div class="radio-input-wrapper">
                                <input type="radio" name="nitro" value="nie" class="normal-radio-input" id="nitro-false">
                            </div>
                            <p class="radio-text">Nie</p>
                        </div>
                    </div>
                    
                    <label for="engine">Silnik*:</label>
                    <input type="text" name="engine" class="text-input required" required="required">
                    <label for="power">Moc silnika*:</label>
                    <input type="number" name="power" class="text-input required" required="required">
                    <label for="torque" class="drift">Moment obrotowy*:</label>
                    <input type="number" name="torque" class="text-input drift required" required="required">
                    <label for="drivetrain" class="zlot">Rodzaj napędu*:</label>
                    <div class="select-wrapper zlot">
                        <select name="drivetrain" id="category-select">
                            <option value="Przód">Przód</option>
                            <option value="Tył">Tył</option>
                            <option value="4x4">4X4</option>
                        </select>
                    </div>
                    
                    <div class="drift radio-wrapper" id="license">
                        <label for="licence">Licencja zawodnika*:</label>
                        <div class="radio-input-wrapper checked">
                            <input type="radio" name="licence" value="tak" class="normal-radio-input" checked required="required" id="license-true">
                        </div>
                        <p class="radio-text">Tak</p>
                        <div class="radio-input-wrapper">
                            <input type="radio" name="licence" value="nie" class="normal-radio-input" id="license-false">
                        </div>
                        <p class="radio-text">Nie</p>
                    </div>
                    
                    <label for="osiagniecia" class="drift">Osiągnięcia*:</label>
                    <textarea name="osiagniecia" rows="10" class="drift" maxlength="300"></textarea>
                    <label for="car-description" class="zlot zlot-header">Opis samochodu:</label>
                    <p class="smaller zlot">(max. ilość znaków 300) (np. kolor, unikatowe cechy)</p>
                    <textarea name="car-description" rows="10" class="zlot" maxlength="300"></textarea>
                    <label for="modifications" class="tuning" id="tuning-header">Modyfikacje samochodu:</label>
                    <textarea name="modifications" rows="10" id="tuning" maxlength="300"></textarea>
                    
                    <label for="files">Dodaj 3 zdjęcia*:</label>
                    <p class="smaller files-label">min. 1024 x 800px , max rozmiar 2 MB</p>
                    <div class="files">
                        <div class="file-wrapper" id="file1">
                            <input type="file" accept="image/*" name="file1" class="file-input required" required="required">
                            <p class="text">Dodaj zdjęcie</p>
                        </div>
                        <div class="file-wrapper" id="file2">
                            <input type="file" accept="image/*" name="file2" class="file-input">
                            <p class="text">Dodaj zdjęcie</p>
                        </div>
                        <div class="file-wrapper" id="file3">
                            <input type="file" accept="image/*" name="file3" class="file-input">
                            <p class="text">Dodaj zdjęcie</p>
                        </div>
                    </div>
                    
                    <div class="agree" id="agree">
                        <div class="input-container">
                            <div class="input-wrapper">
                                <input type="checkbox" name="regulamin" class="square-check" required="required" id="regulamin">
                            </div>
                            <label for="regulamin" class="drift">Zapoznałem się z <a href="assets/download/regulamin_drift.pdf" target="_blank">Regulaminem</a>*:</label>
                            <label for="regulamin" class="zlot">Zapoznałem się z <a href="assets/download/regulamin_zgloszenie.pdf" target="_blank">Regulaminem</a>*:</label>
                        </div>
                        <div class="input-container">
                            <div class="input-wrapper">
                                <input type="checkbox" name="zgoda" class="square-check" required="required" id="dane">
                            </div>
                            <label for="zgoda">Wyrażam zgodę na przetwarzanie moich danych osobowych do celu rejestracji*:</label>
                        </div>
                    </div>
                    
                    <div class="submit-wrapper">
                        <input type="submit" name="submit" value="Wyślij" class="block-btn">
                    </div>
                    <div class="c"></div>
                </form>
            </div>
            <div class="contact-col">
                <h3>Kontakt</h3>
                <p class="ceo">Rafał Haznar</p>
                <p>
                    +48 781 475 457<br />
                    speedland@carera.tv
                </p>
            </div>
            <div class="c"></div>
        </div>
        <footer class="footer">
            <div class="container">
                Projekt i realizacja: <a href="http://www.lemonadestudio.pl" target="_blank"><img src="/assets/img/footer-logo.png" alt="Lemonade Studio" /></a>
            </div>
        </footer>
        <script src="https://use.typekit.net/rjl0wsm.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
        <script src="assets/js/jquery-3.2.1.min.js"></script>
        <script src="assets/js/scripts.js?ver=13"></script>
    </body>
</html>
