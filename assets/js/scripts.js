$(function() {
    init();
});

function init() {
    $('#appointment-type-trigger').on('click', function() {
        toggleForms();
    }); 
    $(document).on('click', '#category-select', function() {
        toggleDescriptionInput();
    });
    addFileName();
    enableSpinner();
    hideMessage();
    toggleChecked();
    toggleRadio();
    toggleCustomRadio();
}

/**
 * Toggles between diffrent forms, depending on clicked button.
 */
function toggleForms() {
    if ($('#drift-trigger').is(":checked")) {
        $('.drift').css('display', 'block');
        $('.agree label.drift').css('display', 'inline-block');
        $('.zlot').hide();
        $('.block-btn').removeClass('alt-block-btn');
        $('input.zlot.required').removeAttr('required');
        $('input.drift.required').attr('required', 'required');
    } else if ($('#zlot-trigger').is(":checked")) {
        $('.drift').hide();
        $('.zlot').css('display', 'block');
        $('.block-btn').addClass('alt-block-btn');
        $('input.drift.required').removeAttr('required');
        $('input.zlot.required').attr('required', 'required');
        $('.agree label.zlot').css('display', 'inline-block');
    }
}

/**
 * changing textarea in form.
 */
function toggleDescriptionInput() {
    var selected = $('#category-select option:selected').attr('value');
    if (selected == 'Tuning') {
        $('textarea#tuning').css('display', 'block');
        $('label#tuning-header').css('display', 'block');
    } else {
        $('textarea#tuning').css('display', 'none');
        $('label#tuning-header').css('display', 'none');
    }
}

function addFileName() {
    $('input.file-input').on('change', function() {
        var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
        var current = '#' + $(this).parent().attr('id') + ' .text';
        
        $($(current)).html(filename);
    });
}

function enableSpinner() {
    $('.form').on('submit', function() {
        $('.spinner').css('display', 'flex');
    });
}

function hideMessage() {
    $('#message').delay(5000).slideUp(500);
}

function toggleChecked() {
    var buttons = [
        '#drift-trigger',
        '#zlot-trigger'
    ];
    
    $.each(buttons, function(index, value) {
        $(value).parent().on('click', function() {
            var childInput = $(this).children();
            
            $(childInput).prop('checked', true);
            $('.checked').removeClass('checked');
            $(this).toggleClass('checked');
        });
    });
}

function toggleCustomRadio() {
    var buttons = [
        '#regulamin',
        '#dane'
    ];
    
    $.each(buttons, function(index, value) {
        $(value).parent().on('click', function() {
            var input = $(this).children();
            
            $(input).prop('checked', true);
            $(this).toggleClass('input-wrapper-checked');
        });
    });
}

function toggleRadio() {
    var turboButtons = [
        '#turbo-true',
        '#turbo-false'
    ];
    var nitroButtons = [
        '#nitro-true',
        '#nitro-false'
    ];
    var licenseButtons = [
        '#license-true',
        '#license-false'
    ];
    
    iterateThrough(turboButtons, '#turbo');
    iterateThrough(nitroButtons, '#nitro');
    iterateThrough(licenseButtons, '#license');
}

/**
 * Iterates through given array and performs check/uncheck task on elements of it (radio inputs).
 * @param array array
 * @param string id
 */
function iterateThrough(array, id) {
    $.each(array, function(index, value) {
        $(value).parent().on('click', function() {
            var childInput = $(this).children();
            
            $(childInput).prop('checked', true);
            $(id + ' .checked').removeClass('checked');
            $(this).addClass('checked');
        });
    });
}


    