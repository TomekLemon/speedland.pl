<html>
    <h2>Zgłoszenie na DRIFT</h2>
    <p><strong>Dane uczestnika:</strong></p>
    <ul>
        <li><strong>Imię: {name}</strong></li>
        <li><strong>Nazwisko: {surname}</strong></li>
        <li><strong>Email: {email}</strong></li>
        <li><strong>Telefon: {phone}</strong></li>
        <li><strong>Miasto: {city}</strong></li>
        <li><strong>Team: {team}</strong></li>
    </ul>
    <p><strong>Samochód:</strong></p>
    <ul>
        <li><strong>Marka: {car}</strong></li>
        <li><strong>Model: {carmodel}</strong></li>
        <li><strong>Turbo: {turbo}</strong></li>
        <li><strong>Nitro: {nitro}</strong></li>
        <li><strong>Silnik: {engine}</strong></li>
        <li><strong>Moc: {power}</strong></li>
        <li><strong>Moment obrotowy: {torque}</strong></li>
        <li><strong>Rodzaj napędu: {drivetrain}</strong></li>
        <li><strong>Licencja zawodnika: {licence}</strong></li>
        <li><strong>Osiągnięcia: {osiagniecia}</strong></li>
    </ul>
</html>