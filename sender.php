<?php
ob_start();
session_start();
require 'PHPMailerAutoload.php';

$mail = new PHPMailer;
$data = $_POST;
$files = $_FILES;
if ($data['type'] == 'drift') {
    include 'htmlemail_drift.php';
} else {
    include 'htmlemail_zlot.php';
}
$body = ob_get_clean();
$email_vars = array();

foreach ($data as $key => $value) {
    $email_vars[$key] = $value;
}
if ($data['type'] == 'drift') {
    $body = file_get_contents('htmlemail_drift.php');
} else {
    $body = file_get_contents('htmlemail_zlot.php');
}
if(isset($email_vars)){
    foreach($email_vars as $k=>$v){
        $body = str_replace('{'. $k .'}', $v, $body);
    }
}

$mail->CharSet = 'utf-8';
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'lemonade.nazwa.pl';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'mailer@speedland.pl';                 // SMTP username
$mail->Password = 'Zgloszenie!01';                           // SMTP password
$mail->Port = 587;

$mail->setFrom('mailer@speedland.pl', 'Mailer');
//if ($data['type'] == 'drift') {
//    $mail->addAddress('drift@speedland.pl');     // Add a recipient
//} else {
//    $mail->addAddress('zlot@speedland.pl');     // Add a recipient
//}
$mail->addAddress('tomek@lemonadestudio.pl');     // Add a recipient
$mail->addReplyTo($data['email'], $data['name'] . ' ' . $data['surname']);

$mail->addAttachment($files['file1']['tmp_name'], $files['file1']['name']);    // Optional name
$mail->addAttachment($files['file2']['tmp_name'], $files['file2']['name']);    // Optional name
$mail->addAttachment($files['file3']['tmp_name'], $files['file3']['name']);    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML
foreach ($files as $file) {
    if ($file['size'] > 2000000 || $file['error'] > 0) {
        $_SESSION['error'] = 'Wgrywany przez Ciebie plik jest zbyt duży. Nie może mieć więcej niż 2MB.';
        echo "<script type='text/javascript'>
            window.location='http://speedland.loc/';
        </script>";
        die();
    }
}

if ($data['type'] == 'drift') {
    $mail->Subject = 'Speedland formularz zgłoszeniowy DRIFT';
} else {
    $mail->Subject = $data["category"] . ' - Speedland formularz zgłoszeniowy ZLOT';
}
$mail->Body = $body;

if(!$mail->send()) {
    $_SESSION['error'] = 'Wystąpił błąd spróbuj ponownie później.';
} else {
    $_SESSION['success'] = 'Wiadomość została wysłana.';
    echo "<script type='text/javascript'>
        window.location='http://speedland.loc/';
    </script>";
}

