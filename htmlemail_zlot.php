<html>
    <h2>Zgłoszenie na ZLOT - {category}</h2>
    <p><strong>Dane uczestnika:</strong></p>
    <ul>
        <li><strong>Kategoria: {category}</strong></li>
        <li><strong>Imię: {name}</strong></li>
        <li><strong>Nazwisko: {surname}</strong></li>
        <li><strong>Email: {email}</strong></li>
        <li><strong>Telefon: {phone}</strong></li>
        <li><strong>Miasto: {city}</strong></li>
    </ul>
    <p><strong>Samochód:</strong></p>
    <ul>
        <li><strong>Marka: {car}</strong></li>
        <li><strong>Model: {carmodel}</strong></li>
        <li><strong>Rok produkcji: {production-year}</strong></li>
        <li><strong>Numer rejestracyjny: {plate}</strong></li>
        <li><strong>Silnik: {engine}</strong></li>
        <li><strong>Moc: {power}</strong></li>
        <li><strong>Rodzaj napędu: {drivetrain}</strong></li>
        <li><strong>Opis samochodu: {car-description}</strong></li>
        <li><strong>Modyfikacje samochodu: {modifications}</strong></li>
    </ul>
    
    <p><strong>Dodatkowe informacje:</strong></p>
    <ul>
        <li><strong>Profil Facebook: {facebook}</strong></li>
        <li><strong>Klub/ekipa: {team}</strong></li>
    </ul>
    
</html>